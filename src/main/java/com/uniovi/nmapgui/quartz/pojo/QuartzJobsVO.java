package com.uniovi.nmapgui.quartz.pojo;

import lombok.Data;

@Data
public class QuartzJobsVO {
    private String jobDetailName;
    private String jobCronExpression;
    private String timeZone;
    private String groupName;

}
