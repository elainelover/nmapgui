package com.uniovi.nmapgui.quartz.Controller;

import com.uniovi.nmapgui.quartz.StartOfDayJob;
import com.uniovi.nmapgui.quartz.service.JobService;
import com.uniovi.nmapgui.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class JobController {

    @Autowired
    private JobService jobService;
    //获取所有定时任务列表
    @GetMapping("/nmap/getAllJob")
    @ResponseBody
    public R getAllJob() {

        return R.ok(jobService.getAllJob());
    }

    //添加定时任务
    @GetMapping("/nmap/addSchJob")
    @ResponseBody
    public  R addSchJob(String jobName,String code , String corn) {

        Class c2 = StartOfDayJob.class;
        jobService.addJob(jobName,"testuser",c2,corn,code);
        return R.ok();
    }

    /**
     * 创建cron任务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/cron",method = RequestMethod.POST)
    public R startCronJob(@RequestParam("jobName") String jobName, @RequestParam("jobGroup") String jobGroup){
        //jobService.addJob(jobName,jobGroup);
        return R.ok();
    }

    /**
     * 创建异步任务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/async",method = RequestMethod.POST)
    public R startAsyncJob(@RequestParam("jobName") String jobName, @RequestParam("jobGroup") String jobGroup){
        //jobService.addAsyncJob(jobName,jobGroup);
        return R.ok();
    }

    /**
     * 暂停任务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/pause",method = RequestMethod.POST)
    public R pauseJob(@RequestParam("jobName") String jobName, @RequestParam("jobGroup") String jobGroup){
        jobService.pauseJob(jobName,jobGroup);
        return R.ok();
    }

    /**
     * 恢复任务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/resume",method = RequestMethod.POST)
    public R resumeJob(@RequestParam("jobName") String jobName, @RequestParam("jobGroup") String jobGroup){
        jobService.resumeJob(jobName,jobGroup);
        return R.ok();
    }

    /**
     * 删除务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete",method = RequestMethod.PUT)
    public R<Object> deleteJob(@RequestParam("jobName") String jobName, @RequestParam("jobGroup") String jobGroup){
        jobService.deleteJob(jobName,jobGroup);
        return R.ok();
    }
}
