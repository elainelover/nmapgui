package com.uniovi.nmapgui.quartz.service;

import com.uniovi.nmapgui.quartz.pojo.QuartzJobsVO;
import org.quartz.Job;

import java.util.List;

/**
 * @author hzb
 * @date 2018/08/28
 */
public interface JobService {
    /**
     * 添加一个定时任务
     * @param jobName
     * @param jobGroup
     */

    void addJob(String jobName, String jobGroup, Class<? extends Job> cls,String corn,String ip);


    void addAsyncJob(String jobName, String jobGroup, Class<? extends Job> cls);

    //获取在执行的定时任务
    List<QuartzJobsVO> getAllJob();

    /**
     * 暂停任务
     * @param jobName
     * @param jobGroup
     */
    void pauseJob(String jobName, String jobGroup);

    /**
     * 恢复任务
     * @param triggerName
     * @param triggerGroup
     */
    void resumeJob(String triggerName, String triggerGroup);

    /**
     * 删除job
     * @param jobName
     * @param jobGroup
     */
    void deleteJob(String jobName, String jobGroup);


}