package com.uniovi.nmapgui.quartz;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.uniovi.nmapgui.model.Command;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;


@Component
public class StartOfDayJob implements Job {
    @Value("${server.port}")
    String port;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        System.out.println("执行定时函数了。。。。");
        // 添加定时任务：执行核查
       //解析参数
        //请求结果
       String code = jobExecutionContext.getJobDetail().getJobDataMap().getString("code");
       String jobName = jobExecutionContext.getJobDetail().getJobDataMap().getString("jobName");
       String url = "http://127.0.0.1:"+port+"/newNmap-exe?code="+code+"&summary="+jobName;
        try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {
                HttpGet httpGet = new HttpGet(url);

                client = HttpClients.createDefault();
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity);
                System.out.println(result);
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
