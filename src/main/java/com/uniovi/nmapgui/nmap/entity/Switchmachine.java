package com.uniovi.nmapgui.nmap.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2021-02-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Switchmachine implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交换机id
     */
    private String switchId;

    /**
     * 交换机团体名
     */
    private String name;

    /**
     * 查询oid
     */
    private String oid;

    private String taskId;

    /**
     * 交换机ip
     */
    private String ip;

    /**
     * 备注
     */
    private String remarks;


}
