package com.uniovi.nmapgui.nmap.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.UUID;

import com.uniovi.nmapgui.model.Command;
import com.uniovi.nmapgui.model.Output;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-12-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Commandnested implements Serializable {

    private static final long serialVersionUID = 1L;

    private String text;

    private Boolean finished;

    private String output;

    @TableField("chkUpdateFlag")
    private Boolean chkUpdateFlag;

    @TableId("commandNestedId")
    private String commandNestedId;

    public Commandnested(){}
    public Commandnested(Command command){

        this.text = command.getText();
        this.finished = command.isFinished();
        this.output = JSON.toJSONString(command.getOutput());
        this.chkUpdateFlag = command.isChkUpdateFlag();
        this.commandNestedId = UUID.randomUUID().toString();
    }
    public Commandnested(Command command,String uuid){

        this.text = command.getText();
        this.finished = command.isFinished();
        this.output = JSON.toJSONString(command.getOutput());
        this.chkUpdateFlag = command.isChkUpdateFlag();
        this.commandNestedId = uuid;
    }
    public Command getCommand(){

        //将现有的Commandnested对象 转化为  Command对象
        Command command = new Command();
        command.setText(this.text);
        command.setFinished(this.finished);
        command.setOutput(JSON.parseObject(this.output, Output.class));
        command.setChkUpdateFlag(this.chkUpdateFlag);
        return command;
    }
}
