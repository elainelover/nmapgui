package com.uniovi.nmapgui.nmap.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TaskIp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 任务id
     */
    private Integer taskId;

    /**
     * ip
     */
    private String ip;

    /**
     * 端口及服务，以json 的形式保存
     */
    private String port;

    /**
     * 操作系统
     */
    private String os;

    /**
     * mac地址
     */
    private String mac;

}
