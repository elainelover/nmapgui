package com.uniovi.nmapgui.nmap.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;


import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 开始时间
     */
    private String starttime;

    /**
     * 结束时间
     */
    private String endtime;

    /**
     * 核查类型：0 手动 1 自动
     */
    private Integer type;

    /**
     * 文件名称
     */
    private String filename;

    private String summary;

    private String code;

    private int status;

    private String command;

    //克隆函数
    public static Task CloneTask(Task task){

        Task taskOld = new Task();
        taskOld.setId(task.getId());
        taskOld.setStarttime(task.getStarttime());
        taskOld.setEndtime(task.getEndtime());
        taskOld.setType(task.getType());
        taskOld.setFilename(task.getFilename());
        taskOld.setSummary(task.getSummary());
        taskOld.setCode(task.getCode());
        taskOld.setStatus(task.getStatus());
        return taskOld;
    }

}
