package com.uniovi.nmapgui.nmap.service.impl;

import com.uniovi.nmapgui.nmap.entity.Switchmachine;
import com.uniovi.nmapgui.nmap.mapper.SwitchmachineMapper;
import com.uniovi.nmapgui.nmap.service.ISwitchmachineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-02-03
 */
@Service
public class SwitchmachineServiceImpl extends ServiceImpl<SwitchmachineMapper, Switchmachine> implements ISwitchmachineService {

}
