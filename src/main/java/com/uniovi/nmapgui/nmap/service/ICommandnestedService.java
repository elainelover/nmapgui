package com.uniovi.nmapgui.nmap.service;

import com.uniovi.nmapgui.model.Command;
import com.uniovi.nmapgui.nmap.entity.Commandnested;
import com.baomidou.mybatisplus.extension.service.IService;
import com.uniovi.nmapgui.nmap.entity.Task;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-12-24
 */
public interface ICommandnestedService extends IService<Commandnested> {

    void saveToMysql(Commandnested command,Task task);

    void saveToMysqlNew(Command command, Task task);

    List<Command> changeCommandnestTOCommand(List<Commandnested> list);
}
