package com.uniovi.nmapgui.nmap.service.impl;

import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.uniovi.nmapgui.nmap.mapper.TaskIpMapper;
import com.uniovi.nmapgui.nmap.service.ITaskIpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
@Service
public class TaskIpServiceImpl extends ServiceImpl<TaskIpMapper, TaskIp> implements ITaskIpService {

}
