package com.uniovi.nmapgui.nmap.service;

import com.uniovi.nmapgui.nmap.entity.Switchmachine;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-02-03
 */
public interface ISwitchmachineService extends IService<Switchmachine> {

}
