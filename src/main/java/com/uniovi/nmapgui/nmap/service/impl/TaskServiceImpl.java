package com.uniovi.nmapgui.nmap.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.uniovi.nmapgui.nmap.entity.Task;
import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.uniovi.nmapgui.nmap.mapper.TaskIpMapper;
import com.uniovi.nmapgui.nmap.mapper.TaskMapper;
import com.uniovi.nmapgui.nmap.service.ITaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

    @Resource
    TaskMapper taskMapper;
    @Resource
    TaskIpMapper taskIpMapper;

    @Transactional
    @Override
    public boolean deleteTask(int[] ids) {

        try {
            //根据id删除  审查记录
            for (int i = 0; i < ids.length; i++) {

                taskMapper.deleteById(ids[i]);
                //删除i详情
                TaskIp taskIp = new TaskIp();
                taskIp.setTaskId(ids[i]);
                QueryWrapper queryWrapper = new QueryWrapper(taskIp);
                taskIpMapper.delete(queryWrapper);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
