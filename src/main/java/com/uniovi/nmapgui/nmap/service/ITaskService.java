package com.uniovi.nmapgui.nmap.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.uniovi.nmapgui.nmap.entity.Task;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
public interface ITaskService extends IService<Task> {

    boolean deleteTask(int[] id);
}
