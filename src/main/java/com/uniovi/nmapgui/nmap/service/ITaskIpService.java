package com.uniovi.nmapgui.nmap.service;

import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
public interface ITaskIpService extends IService<TaskIp> {

}
