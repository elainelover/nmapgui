package com.uniovi.nmapgui.nmap.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.uniovi.nmapgui.model.Command;
import com.uniovi.nmapgui.model.Host;
import com.uniovi.nmapgui.nmap.entity.Commandnested;
import com.uniovi.nmapgui.nmap.entity.Task;
import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.uniovi.nmapgui.nmap.mapper.CommandnestedMapper;
import com.uniovi.nmapgui.nmap.service.ICommandnestedService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uniovi.nmapgui.nmap.service.ITaskIpService;
import com.uniovi.nmapgui.nmap.service.ITaskService;
import com.uniovi.nmapgui.util.JsoupManger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-12-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CommandnestedServiceImpl extends ServiceImpl<CommandnestedMapper, Commandnested> implements ICommandnestedService {

    @Resource
    CommandnestedMapper commandnestedMapper;
    @Autowired
    ITaskService iTaskServiceImlp;
    @Autowired
    ITaskIpService iTaskIpServiceImlp;

    @Transactional
    @Override
    public void saveToMysql(Commandnested command, Task task) {

        Task t = iTaskServiceImlp.getById(task.getId());
        if (t == null) {
            return;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        t.setEndtime(simpleDateFormat.format(new Date()));
        t.setStatus(0);
        iTaskServiceImlp.updateById(t);
        //解析xml数据 并保存至副表
        List<TaskIp> taskIps = JsoupManger.getMesFromHtml((JSONObject.parseObject(command.getOutput()).getString("xml")), t.getId());
        //System.out.println(taskIps.size());
        for (int i = 0; i < taskIps.size(); i++) {
            iTaskIpServiceImlp.save(taskIps.get(i));
        }
        //将检测结果保存为log文件

    }

    @Override
    public void saveToMysqlNew(Command command, Task task) {
        if (command.getText() != null) {
            Commandnested commandnested = new Commandnested();
            commandnested.setOutput(JSON.toJSONString(command.getOutput()));
            QueryWrapper wrapper = new QueryWrapper<>(commandnested);
            Commandnested commandnestedOld = commandnestedMapper.selectOne(wrapper);
            if (commandnestedOld == null) {

                //当前数据库中并没有 此条检测结果  将结果保存
                Commandnested commandnestedNew = new Commandnested(command);
                commandnestedMapper.insert(commandnestedNew);
                QueryWrapper queryWrapper = new QueryWrapper(task);
                Task t = iTaskServiceImlp.getById(task.getId());
                if (t == null) {
                    return;
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                t.setEndtime(simpleDateFormat.format(new Date()));
                iTaskServiceImlp.updateById(t);
                //解析xml数据 并保存至副表
                List<TaskIp> taskIps = JsoupManger.getMesFromHtml(command.getOutput().getXml(), t.getId());
                //System.out.println(taskIps.size());
                for (int i = 0; i < taskIps.size(); i++) {
                    iTaskIpServiceImlp.save(taskIps.get(i));
                }
            }
        }
    }

    @Override
    public List<Command> changeCommandnestTOCommand(List<Commandnested> list) {

        List<Command> commands = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {

            commands.add(list.get(i).getCommand());
        }
        return commands;
    }

}
