package com.uniovi.nmapgui.nmap.mapper;

import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
public interface TaskIpMapper extends BaseMapper<TaskIp> {

}
