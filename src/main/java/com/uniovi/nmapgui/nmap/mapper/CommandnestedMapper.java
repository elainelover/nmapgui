package com.uniovi.nmapgui.nmap.mapper;

import com.uniovi.nmapgui.nmap.entity.Commandnested;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-12-24
 */
public interface CommandnestedMapper extends BaseMapper<Commandnested> {

}
