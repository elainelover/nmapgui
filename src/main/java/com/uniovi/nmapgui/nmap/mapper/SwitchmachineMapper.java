package com.uniovi.nmapgui.nmap.mapper;

import com.uniovi.nmapgui.nmap.entity.Switchmachine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-02-03
 */
public interface SwitchmachineMapper extends BaseMapper<Switchmachine> {

}
