package com.uniovi.nmapgui.nmap.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.uniovi.nmapgui.nmap.entity.TaskIp;
import com.uniovi.nmapgui.nmap.service.ITaskIpService;
import com.uniovi.nmapgui.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/nmap")
public class TaskIpController {

    @Autowired
    ITaskIpService taskIpServiceImpl;
    //  获取 核查详情
    @GetMapping("/taskIpList")
    @ResponseBody
    public R taskIpList(int pageSize, int pageNum, int taskId) {

        IPage page = new Page(pageNum,pageSize);
        TaskIp taskIp = new TaskIp();
        taskIp.setTaskId(taskId);
        QueryWrapper<TaskIp> queryWrapper = new QueryWrapper(taskIp);
        IPage taskIpPage = taskIpServiceImpl.page(page,queryWrapper);

        return R.ok(taskIpPage);
    }

}
