package com.uniovi.nmapgui.nmap.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.uniovi.nmapgui.nmap.entity.Switchmachine;
import com.uniovi.nmapgui.nmap.service.ISwitchmachineService;
import com.uniovi.nmapgui.nmap.service.impl.SwitchmachineServiceImpl;
import com.uniovi.nmapgui.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2021-02-03
 */
@RestController
@RequestMapping("/nmap/switchmachine")
public class SwitchmachineController {

    @Autowired
    ISwitchmachineService switchmachineServiceImpl;
    @PostMapping("addSwitchMachine")
    @ResponseBody
    public R addSwitchMachine(Switchmachine switchmachine){

        //添加交换机
        switchmachine.setSwitchId(UUID.randomUUID().toString());
        switchmachineServiceImpl.save(switchmachine);
        return  R.ok();
    }

    //获取交换机列表
    @PostMapping("getSwitchMachines")
    @ResponseBody
    public R getSwitchMachines(int pageSize , int pageNum){

        IPage page = new Page(pageNum,pageSize);
        //查询交换机
        return  R.ok(switchmachineServiceImpl.page(page));
    }

}
