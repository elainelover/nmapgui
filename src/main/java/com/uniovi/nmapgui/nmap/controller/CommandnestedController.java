package com.uniovi.nmapgui.nmap.controller;


import com.uniovi.nmapgui.model.Command;
import com.uniovi.nmapgui.nmap.entity.Commandnested;
import com.uniovi.nmapgui.nmap.service.ICommandnestedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-12-24
 */
@Controller
public class CommandnestedController {

    @Autowired
    ICommandnestedService iCommandnestedServiceImpl;

    @GetMapping("/nmap/history")
    public String finishedList(Model model) {

        List<Command> finishedCommands = iCommandnestedServiceImpl.changeCommandnestTOCommand(iCommandnestedServiceImpl.list());
        for(int i = 0 ; i < finishedCommands.size() ; i++){
            model.addAttribute("command", new Command());
        }
        model.addAttribute("finishedCommands", finishedCommands);
        return "fragments/contents :: finished";
    }

    @ResponseBody
    @GetMapping("/nmap/test")
    public String finishedList() {

        return "请求到啦！";
    }
}
