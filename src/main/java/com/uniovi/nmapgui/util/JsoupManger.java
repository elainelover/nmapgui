package com.uniovi.nmapgui.util;

import com.alibaba.fastjson.JSON;
import com.uniovi.nmapgui.nmap.entity.TaskIp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class JsoupManger {

    //jsoup 工具类
    public static List<TaskIp> getMesFromHtml(String html,int taskId) {

        //***********************使用url获取文档
		/*String url="http://www.gzmssy.cn";
		try {
			Document document = Jsoup.connect(url).get();
			System.out.println(document.title()); // 获得文档标题
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        //***********************
        //url = "D:\\html.html";
        List<TaskIp> taskIps = new ArrayList<>();
        //***********************使用文件加载html文档
        //File html = new File(url);
        //将文件整理
        html.replace("\r","").replace("\n","");
        Document document2 = Jsoup.parse(html, "utf-8");
        Elements unhidden = document2.getElementsByClass("unhidden");
        Iterator<Element> it = unhidden.iterator();
        //System.out.println(document2.getElementsByClass("unhidden"));	// 获得div
        while (it.hasNext()) {
            TaskIp taskIp = new TaskIp();
            Element div = it.next();
            Elements tables = div.getElementsByTag("table");
            Elements osElement = div.getElementsByTag("li");
            String os = "";
            String ip = "";
            List<Map<String, String>> lists = new ArrayList<>();
            //System.out.println(os.size());
            //获得os
            for (int i = 0; i < osElement.size(); i++) {
                try {
                    if (osElement.get(i).ownText().contains("OS match:")) {

                        //解析一个相似度最高的  操作系统
                        os = osElement.get(i).getElementsByTag("b").get(0).ownText();
                        break;
                        //System.out.println(os);
                    }
                } catch (Exception e) {

                }
            }
            //获得 table
            for (int i = 0; i < tables.size(); i++) {

                Element table = tables.get(i);
                //System.out.println(table.id());
                if (table.id() != null && table.id().contains("porttable")) {
                    try {
                        Elements trs = table.getElementsByTag("tr");
                        //获取open
                        for (int j = 0; j < trs.size(); j++) {

                            if ("open".equals(trs.get(j).className()) || "filtered".equals(trs.get(j).className()) ||
                                    "closed".equals(trs.get(j).className()) || "unfiltered".equals(trs.get(j).className())) {

                                Map<String, String> map = new HashMap<>();
                                //解析每个接口状态
                                //System.out.println(trs.get(j).children().get(0).ownText());
                                map.put("port", trs.get(j).children().get(0).ownText());
                                map.put("type", trs.get(j).children().get(1).ownText());
                                map.put("state", trs.get(j).children().get(2).ownText());
                                map.put("service", trs.get(j).children().get(3).ownText());
                                map.put("reason", trs.get(j).children().get(4).ownText());
                                map.put("product", trs.get(j).children().get(5).ownText());
                                map.put("version", trs.get(j).children().get(6).ownText());
                                map.put("extrainfo", trs.get(j).children().get(7).ownText());
                                lists.add(map);
                            }
                        }
                        //获取ip
                        ip = table.id().replace("porttable_", "");
                        //System.out.println(ip);
                    } catch (Exception e) {
                        System.out.println("本次  解析出错");
                    }
                }
            }
            taskIp.setOs(os);
            taskIp.setPort(JSON.toJSONString(lists));
            taskIp.setIp(ip);
            taskIp.setTaskId(taskId);
            taskIps.add(taskIp);
        }
        return  taskIps;
        //***********************

        //***********************从String中加载HTML文档
       /* String html2 = "<html><head><title>Jsoup 标题</title></head>"
                + "<body><p>Parsed HTML into a doc.</p></body></html>";
        Document document3 = Jsoup.parse(html2);
        System.out.println(document3.title());	// 获得文档标题*/

        //***********************

    }

}
