package com.uniovi.nmapgui;

import com.uniovi.nmapgui.quartz.QuartzManager;
import com.uniovi.nmapgui.quartz.StartOfDayJob;
import com.uniovi.nmapgui.quartz.service.JobService;
import org.mybatis.spring.annotation.MapperScan;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;


@SpringBootApplication
@EnableAsync
@MapperScan({"com.uniovi.nmapgui.nmap.mapper"})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableScheduling
public class NMapGuiApplication extends AsyncConfigurerSupport {

/*	@Autowired
	JobDetail job;
	@Autowired
	Trigger trigger;
	@Autowired
	JobService jobServiceImpl;*/

	public static ConfigurableApplicationContext mainExec(String[] args) {
    	
		return SpringApplication.run(NMapGuiApplication.class, args);
	}

	public static void main(String[] args) {
		mainExec(args);
	}
	//@PostConstruct
	public void TestQuartz() throws SchedulerException {
		/*System.out.println("@Postcontruct’在依赖注入完成后自动调用");
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		//加入这个调度
		scheduler.scheduleJob(job, trigger);
		//启动之
		scheduler.start();*/
		//quartzManager.addJob("test", StartOfDayJob.class,"* * * * * ?");
		Class c2 = StartOfDayJob.class;
		//jobServiceImpl.addJob("test","testuser",c2,"*/2 * * * * ?");
		//jobServiceImpl.pauseJob("test","testuser");
		//jobServiceImpl.resumeJob("test","testuser");
		//jobServiceImpl.deleteJob("test","testuser");
	}
}
